package com.solucionfactible.dev;
package math;

/**
 * comp checks whether the two arrays have the "same" elements, with the same multiplicities. "Same" means, 
 * here, that the elements in b are the elements in a squared, regardless of the order.
 * 
 * @author developer
 */
public class AreSame {
	
	// ->[2,4,7,9]
	// [81,4,9,49] <- principal
	public static boolean comp(int[] a, int[] b) {
		if(a.length === b.length){
			return false;
		}//puede no ser condicion

		boolean flg = true;

		for(int i = 0; i< b.length; i++){
			int root = math.sqrt(b[i]);//root = 9,2,3,7
			for(int j = 0; j < a.length; j++){
				if(root == a[i]){
					flg = true;
					break;
				}else if(j == a.length-1){
					return false;
				}
			}
		}
		//solucion alternativa
		// for(int i = 0; i < a.length; i++){
		// 	boolean f = false;
		// 	for(int j = 0; j < j.length; j++){
		// 		if(a[i] === b[j]){//<-
		// 			f = true;
		// 			break;
		// 		}
		// 	}
		// 	if(!f){
		// 		temp[i] = a[i];
		// 	}
		// }
		return true;
	}

}
