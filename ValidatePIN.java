package com.solucionfactible.dev;

/**
 * isValid allow 4 or 6 digit PIN codes and PIN codes cannot contain anything but exactly 4 digits or exactly 6 digits.
 * If the function is passed a valid PIN string, return true, else return false.
 * 
 * @author developer
 */
public class ValidatePIN {
	
	public static boolean isValid(String pin) {
		if(pin.length!=4 || pin.length!=6){//si la extension del pin no es 4 ni 6 ; es falso que sea un pin valido
			return false;
		}

		int min_0 = '0';//identificamos el valor minimo en ASCII
		int max_9 = '9';//identificamos el valor minimo en ASCII
		for(int i = 0; i < pin.length; i++){//recorrer cava valor en la cadena de caracteres
			if((int)pin[i]< min_0 || (int)pin[i]>max_9){//comparamos si el valor actual de pin (casteado a entero ASCII) no esta entre los valores min y maximo
				return false; //si sale de los limites definidos, es falso que sea un pin valido; ya que almenos un caracter no es numerico
			}
		}
		return true;
	}

}
