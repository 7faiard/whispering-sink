package com.solucionfactible.dev;

/**
 * persistence takes in a positive parameter num and returns its multiplicative persistence, which is the number 
 * of times you must multiply the digits in num until you reach a single digit.
 * 
 * @author developer
 */
public class Persistence {
	
	public static int persistence(long num) {
		if(num<10){//si num es menor a 10; no hay elementos para realizar multiplicacion
			return 0;
		}

		String numero = new String(num);//transformamos el numero como una cadena de caracteres;
		int counter = 0; //contador del numero de veces que una multiplicacion se realiza
		while(numero.length>1){//relizaremos multiplicacion mientras existan varias cifras en el numero resultante
			numero = multiplicacion(numero); //ejecutamos las multiplicaciones y retornamos el resultado como un acadena de caracteres String
			counter++;//variable auxiliar para contar cuantas veces se ha hecho la multiplicacion
		}
		return counter; //cuando las cifras en el resultado almacenado en la variable numero sea solo una; retornamos cuantas veces se ejecuto la operacion.
	}

	public String multiplicaiones(String lista){
		int prod = 1;
		int valor_actual;
		for(int i = 0; i <  lista.length; i++){//recoremos los valores de la lista
			valor_actual = (int)lista[i] - 48;//casteamos el primer numero, de char a entero y restamos 48 en ASCII para obtener el valor numerico real
			prod = prod * valor_actual;// realizamos la multiplicacion con el valor actual de la lista hasta termina
		}	
		return new String(prod);//retornamos el valor como una cadena de caracteres
	}
}

