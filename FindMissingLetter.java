package com.solucionfactible.dev;

/**
 * findMissingLetter takes an array of consecutive (increasing) letters as input and that returns the missing letter 
 * in the array. You will always get an valid array. And it will be always exactly one letter be missing. 
 * The length of the array will always be at least 2.The array will always contain letters in only one case.
 * (Use the English alphabet with 26 letters)
 * 
 * @author developer
 */
public class FindMissingLetter {
	
	public static char findMissingLetter(char[] array) {
		char [] = new char[]{'b','c','e'};//{132,133,135};<-
		int unicodeInicial = array[0];//b <- transforma a ASCII number

		for(int i = 0; i < array.length; i++){//i=2
			int currentUni = unicodeInicial+i;//98
			if(currentUni != (int)array[i]){//casteamos el char como entero
				return  (char)currentUni;//100->d
			}
		}
		return ' ';
	}

}
