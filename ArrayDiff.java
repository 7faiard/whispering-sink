package com.solucionfactible.dev;

/**
 * diff subtracts one list from another and returns the result.
 * It should remove all values from list a, which are present in list b keeping their order.
 * 
 * @author developer
 */
public class ArrayDiff {
	
	public static int[] diff(int[] a, int[] b) {
		// a = [1,7,2]
		//b = [2,1,3]
		// r = [1,7]
		int [a.length] temp = new int[]{};
		for(int i = 0; i < a.length; i++){
			boolean f = false;
			for(int j = 0; j < j.length; j++){
				if(a[i] === b[j]){//<-
					f = true;
					break;
				}
			}
			if(!f){
				temp[i] = a[i];
			}
		}
		a = temp;
		return a;
	}

}
